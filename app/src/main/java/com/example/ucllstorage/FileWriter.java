package com.example.ucllstorage;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;

public class FileWriter {

    public static void AppendToFile(String text){
        File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "UCLL" +
                "/files");
        if (!dir.exists()) {
            try {
                if (dir.mkdirs()){
                    System.out.println("dir created");
                } else {
                    System.out.println("somthing went wrong");
                    return;
                }
            } catch (Exception e) {
                System.out.println("dir is de error" + e);
                return;
            }
        }

        File file = new File(dir.getAbsolutePath() + "/file1.txt");
        if (!file.exists()) {
            try {
                if (file.createNewFile()){
                    System.out.println("file created");
                } else {
                    System.out.println("somthing went wrong with file creation");
                    return;
                }
            } catch (IOException e) {
                System.out.println("this is the file error" + e);
                return;
            }
        }

        try(BufferedWriter writer = new BufferedWriter(new java.io.FileWriter(file, true))){
            writer.append(text);
            writer.newLine();
            System.out.println("append was a succes");
        } catch(IOException e){
            System.out.println("append error :" + e);
        }
    }
}
