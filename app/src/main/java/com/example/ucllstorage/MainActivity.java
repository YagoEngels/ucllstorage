package com.example.ucllstorage;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button save, load;
    private RadioButton radioPref,radioFile,radioDatabase;
    private EditText txtInput;
    private TextView txtOutput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        save = findViewById(R.id.btnSave);
        load = findViewById(R.id.btnLoad);

        radioPref = findViewById(R.id.radPref);
        radioFile = findViewById(R.id.radFile);
        radioDatabase = findViewById(R.id.radDatabase);

        txtInput = findViewById(R.id.txtInput);
        txtOutput = findViewById(R.id.txtOutput);

        //functies
        clickSave();
        clickLoad();
    }

    @Override
    public void onRequestPermissionsResult(int reqCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(reqCode, permissions, grantResults);

        if (reqCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                System.out.println("permission granted p2");
            } else {
                Toast.makeText(getApplicationContext(), "Permission NOT granted. Nothing will be logged.", Toast.LENGTH_LONG).show();
            }
        }

    }

    public void clickSave(){
        save.setOnClickListener(view -> {
            //pref checked
            if (radioPref.isChecked()){
                if (!txtInput.getText().toString().isEmpty()){
                    SharedPreferences prefs = this.getSharedPreferences("PrefContent", Context.MODE_PRIVATE);
                    prefs.edit().putString("input", txtInput.getText().toString()).apply();
                    txtInput.setText(" ");
                } else {
                    Toast toast=Toast. makeText(getApplicationContext(),"give input",
                            Toast. LENGTH_SHORT);
                    toast. show();
                }
            }

            //file checked
            if (radioFile.isChecked()){
                if (!txtInput.getText().toString().isEmpty()){
                    if (ContextCompat.checkSelfPermission(getApplicationContext(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        FileWriter.AppendToFile(txtInput.getText().toString());
                        System.out.println("permission granted");
                    } else {
                        ActivityCompat.requestPermissions(MainActivity.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                1);
                    }
                } else {
                    Toast toast = Toast.makeText(getApplicationContext(),"give input",
                            Toast.LENGTH_SHORT);
                    toast.show();
                }
            }


            //database file
            if (radioDatabase.isChecked()){
                if (!txtInput.getText().toString().isEmpty()){
                    txtInput.setText(" ");
                } else {
                    Toast toast=Toast. makeText(getApplicationContext(),"give input",
                            Toast. LENGTH_SHORT);
                    toast. show();
                }
            }
        });
    }

    public void clickLoad(){
        load.setOnClickListener(view -> {
            //pref checked
            if (radioPref.isChecked()){
                SharedPreferences prefs = this.getSharedPreferences("PrefContent", Context.MODE_PRIVATE);
                if (prefs.contains("input")){
                    txtOutput.setText(prefs.getString("input","Preference undefined"));
                }
            }

            //radiofile is checked
            if(radioFile.isChecked()){
                txtOutput.setText("Bestand FileContent.txt niet gevonden");
            }

            //database checked
            if (radioDatabase.isChecked()){
                txtOutput.setText("database niet gevonden");
            }
        });
    }
}